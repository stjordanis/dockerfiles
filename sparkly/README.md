# Docker image `sparkly` 

Used for testing using Spark (2.4.3) from R with the sparklyr package.

## Main features

- R 3.6.1
- openjdk 8
- Apache Spark 2.4.3
- Apache Arrow C++ libraries

R Packages (including dependencies):

- sparklyr (current)
- arrow (current)
- remotes (current)
- microbenchmark (current)

## Usage

### Interactive with R and sparklyr

```
docker run --rm -it jozefhajnala/sparkly:test R

# R session should start
library(sparklyr)
sc <- spark_connect("local")
```

### Interactive with spark shell

```
docker run --rm -it jozefhajnala/spark:test /root/spark/spark-2.4.3-bin-hadoop2.7/bin/spark-shell

# Scala REPL should open with
# - Spark context available as `sc`
# - Spark session avaiable as `spark`
```

### Running an example R script

```
docker run --rm jozefhajnala/sparkly:test Rscript /root/.local/spark_script.R
```
