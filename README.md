# dockerfiles

Repository of custom Dockerfiles used for R work. All the images build on the [Rocker images](https://www.rocker-project.org/images/).

More details in the respective image directories:

- [rdev](https://gitlab.com/jozefhajnala/dockerfiles/tree/master/rdev) - Used for development work with R version 3.4.4
- [rhub](https://gitlab.com/jozefhajnala/dockerfiles/tree/master/rhub) - Used for multi-platform checking of R packages with rhub
- [sparkly](https://gitlab.com/jozefhajnala/dockerfiles/tree/master/sparkly) - Used for testing using Spark (2.4.3) from R with the sparklyr package
