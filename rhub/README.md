# Docker image `rhub`

Used for multi-platform checking of R packages with rhub.

## Main features

- R (base, current)

R Packages (including dependencies):

- rhub (current)

## Usage

- Example usage at the Gitlab CI [pipeline for jhaddins on experimental](https://gitlab.com/jozefhajnala/jhaddins/blob/experimental/.gitlab-ci.yml)
- Read more at [Setting up continuous multi-platform R package building, checking and testing with R-Hub, Docker and GitLab CI/CD for free, with a working example](https://jozef.io/r107-multiplatform-gitlabci-rhub/)

## Notes

- requires a `resources\validated_emails.csv` file to build
- to get `validated_emails.csv`, run `rhub::validate_email()` and it should save that file into `rappdirs::user_data_dir("rhub", "rhub")`
